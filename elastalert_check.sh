#!/bin/bash


FILES=$(find . -type f -name "*.yml")

ERROR_FOUND=false

for file in $FILES; do
  if grep -q 'alert_text:.*\\' "$file"; then
    echo "Error: Backslashes are not allowed in the alert_text field of the file: $file"
    ERROR_FOUND=true
  fi
done


if $ERROR_FOUND; then
  exit 1
fi

echo "All files are in the correct format."
exit 0
